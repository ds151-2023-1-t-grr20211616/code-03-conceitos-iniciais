import React from "react";
import { View, Text, StyleSheet} from 'react-native';

const Topic = ({header, content}) => {
    return (
        <View style={styles.main}>
            <Text style={styles.header}>{header}</Text>
            <Text style={styles.content}>{content}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        margin: 10
    },
    header: {
        fontSize: 15,
        fontWeight: "bold",
    },
    content: {

    }
})

export default Topic