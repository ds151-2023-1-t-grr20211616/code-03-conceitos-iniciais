import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Topic from '../components/Topic.js';

function HomeScreen() {
  return (
    <View>
      <Topic header='Topic 1' content='Texto do Topic 1'/>
      <Topic header='Topic 2' content='Texto do Topic 2'/>
      <Topic header='Topic 3' content='Texto do Topic 3'/>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBold: {
    fontWeight: 'bold',
  }
})

export default HomeScreen;